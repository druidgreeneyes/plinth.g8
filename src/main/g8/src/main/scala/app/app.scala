package app

import plinth.*
import plinth.syntax.*
import plinth.std.*

case class Plugin(
    tagName: String,
    targetRef: Option[String]
)

object app extends Plinth[Plugin] {
  override def run(config: Plugin): Either[CLIError, String] =
    val resolvedRef = config.targetRef match
      case Some(value) => value
      case None =>
        git.head match
          case Right(value) => value
          case Left(e)      => e.raise

    log.info(s"Tag name: \${config.tagName}")
    log.info(s"Target ref: \$resolvedRef")

    git.branch.all
}
