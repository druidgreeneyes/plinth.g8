val scala3Version = "3.2.2"

enablePlugins(ScalaNativePlugin)
enablePlugins(GitlabPlugin)

version := "0.1.0-SNAPSHOT"

scalaVersion := scala3Version

libraryDependencies ++= Seq(
  "io.gitlab.druidgreeneyes" %%% "plinth" % "0.1.5"
)

import scala.scalanative.build.*

// defaults set with common options shown
nativeConfig ~= { c =>
  c.withLTO(LTO.none) // thin
    .withMode(Mode.debug) // releaseFast
    .withGC(GC.immix) // commix
}

gitlabRepositories ++= Seq(
  GitlabProjectRepository(gitlabDomain.value, GitlabProjectId("45185655"))
)
